use inotify::{EventMask, Inotify, WatchMask};
use log::{error, info};
use serde::{Deserialize, Serialize};
use std::{
    fs::read_to_string,
    net::SocketAddr,
    sync::{Arc, RwLock},
};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    pub protocol: i32,
    pub backend: SocketAddr,
    pub bind: SocketAddr,
    pub whitelist: Vec<PlayerConfig>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PlayerConfig {
    pub token: Option<String>,
    pub username: String,
}

pub fn watch(config: Arc<RwLock<Arc<Config>>>) {
    std::thread::spawn(move || {
        let mut inotify = Inotify::init().unwrap();
        inotify
            .watches()
            .add(
                ".",
                WatchMask::MODIFY | WatchMask::CREATE | WatchMask::DELETE,
            )
            .unwrap();
        let mut buffer = [0u8; 4096];
        loop {
            let events = inotify
                .read_events_blocking(&mut buffer)
                .expect("Failed to read inotify events");

            for event in events {
                if event.mask.contains(EventMask::MODIFY) {
                    info!("reloading config");
                    match serde_yaml::from_str::<Config>(&read_to_string("proxy.yaml").unwrap()) {
                        Ok(conf) => *config.write().unwrap() = Arc::new(conf),
                        Err(e) => error!("config has errors: {e}"),
                    }
                }
            }
        }
    });
}
