# trash-proxy

A really bad minecraft reverse proxy for authentification of offline clients.

Whenever a client connects, the username is checked against all tokens in the
whitelist, the first match will determine the username you log in as.

## Features

- Supported protocol version: 1.19.2
- Hot-reload config on changes

## Usage

- Install with `cargo install --path .`
- Write configuration to `./proxy.yaml`
- Run `trash-proxy`

## Example configuration

```
protocol: 760
backend: 127.0.0.1:25567
bind: 0.0.0.0:25565
whitelist:
  - username: Steve
    token: veryverysecret
  - username: Alex
    token: hunter2
```
